%% \documentclass[12pt]{article}
%% \usepackage{epsf,amsmath,amssymb,graphicx,axodraw,longtable}
%% \usepackage{lhchiggs,heppennames2,cernunits}
%% \begin{document}

\section{$\PW\PH$/$\PZ\PH$ production mode\footnote{%
    S.~Dittmaier, R.V.~Harlander, J.~Olsen, G.~Piacquadio (eds.);
    O.~Brein, M.~Kr\"amer, T.~Zirke }}

\subsection{Experimental overview}

Searches for the Higgs boson in the $\PW\PH$ and $\PZ\PH$ production modes,
usually defined as Higgs-strahlung processes, have been considered
mainly by exploiting two decay modes, $\PH \rightarrow \PW^{+}\PW^{-}$ and $\PH
\rightarrow \PQb\bar{\PQb}$. While the former is looked for mainly because it
could contribute to the measurement of the Higgs-boson coupling to $\PW$
bosons, the latter decay mode might contribute to the discovery of a 
low-mass Higgs boson and later allow to measure the coupling of the Higgs
boson to b-quarks. At the LHC the $\PH \rightarrow \PW^{+}\PW^{-}$ decay
mode becomes accessible for Higgs-boson masses above $\approx 155$\UGeV,
while the $\PH \rightarrow \PQb\bar{\PQb}$ decay mode is accessible for 
Higgs-boson masses below $\approx 125$\UGeV.

The $\PW\PH \rightarrow \PW\PW\PW$ channel in the tri-lepton mode was explored
with a parton-level study in \Bref{Baer:1998cm}, while a first
estimate of the discovery sensitivity at the LHC was presented in
\Brefs{atlasphystdr,ATL-PHYS-2000-008}, based on
a fast simulation of the ATLAS detector only. In the latter document the
statistical discovery significance of the ATLAS detector with an
integrated luminosity of $30$\Ufb$^{-1}$ was estimated to be above
$3\sigma$ for Higgs-boson masses in the range $160{-}170$\UGeV. However, a
more realistic study based on samples of fully simulated Monte Carlo
events, presented in \Bref{CSC}, shows that the extraction of this
signal might be significantly harder than previously thought, in
particular due to the very high $\PQt\bar{\PQt}$ background, although a
precise quantitative estimate of the discovery significance suffers from
the limited available statistics of the samples and from the fact that
the continuum $\PW\PW\PW$ background was not considered in the study.

The decay channel $\PH \rightarrow \PQb\bar{\PQb}$ is dominant at low
Higgs-boson masses, below $\approx 130$\UGeV. Since it is not accessible
in combination with the gluon fusion production mode and only marginally
accessible in combination with the vector-boson fusion, most of the
studies rely on the associated production of a Higgs boson either with a
$\PZ$ or $\PW$ boson ($\PW\PH$ or $\PZ\PH$) or with a $\PQt\bar{\PQt}$
pair. The $\PW\PH$ and $\PZ\PH$ channels with $\PH \to \PQb\bar{\PQb}$
are the main search channels at the Tevatron for a Higgs boson with low
mass, but at the LHC it is significantly more challenging to extract
these signals from the backgrounds. A first study of the sensitivity to
a Higgs boson in the $\PW\PH$ and $\PZ\PH$ channels was presented in the
ATLAS TDR~\cite{atlasphystdr} and one year later in
\Brefs{ATL-PHYS-2000-023,ATL-PHYS-2000-024}. The channel with the
most significant predicted signal is $\PW\PH$, which however results in
a predicted discovery significance of $\approx 2$ after
30\Ufb$^{-1}$ and a signal to background ratio of $\approx
2$\%. Under these conditions, the extraction of the signal is extremely
challenging, since the significance is low and the normalization of the
backgrounds in the signal region must be controlled at the \% level.

More recently, in \Bref{Butterworth:2008iy}, it has been proposed
to focus the search for a Higgs boson in the $\PW\PH$ and $\PZ\PH$
channels with the decay $\PH \rightarrow \PQb\bar{\PQb}$ into the very
specific kinematic region where both the Higgs boson and the $\PW$ or
$\PZ$ boson produced in association with it are emitted at high $\pT$
(e.g.\ $\pT>200$\UGeV), i.e.\ in a topological configuration where they
are back-to-back in the transverse plane and highly boosted. As a first
consequence, the intermediate virtual $\PW$ or $\PZ$ boson producing the
Higgs boson and the associated vector boson must be very massive, thus
even with the LHC centre-of-mass energy it will produced quite
centrally, so that the kinematic acceptance of its decay products, the
Higgs and the $\PW$ bosons, will be significantly improved. In addition,
for various reasons, the signal-to-background ratio is significantly
improved, reducing the impact of background uncertainties onto the
discovery significance. A first study based on a realistic simulation of
the ATLAS detector, but based only on $LO$ Monte Carlo generators, was
performed in \Bref{ATL-PHYS-PUB-2009-088}, where it was found that
after 30\Ufb$^{-1}$ of data collected at a centre-of-mass energy of
14\UTeV\ a discovery significance above 3 should be achievable and that
these channels might contribute, in combination with others, to the
discovery of a low-mass Higgs boson with around 10\Ufb$^{-1}$ of
integrated luminosity. The CMS experiment is in the process of reviewing
an analogous analysis, which should be released very soon. One of the
main challenges of these searches is to control the backgrounds down to
a precision of $\approx 10$\% or better in the very specific kinematic
region where the signal is expected. While further studies (e.g.\ in
\Bref{CERN-THESIS-2010-027}) suggest that the $\PQt\bar{\PQt}$
background might be extracted in a signal-free control region, this
might be significantly harder for the $\PW\PQb\bar{\PQb}$ or
$\PZ\PQb\bar{\PQb}$ irreducible backgrounds, where the use of precise
predictions provided by theoretical perturbative calculations seems to
be crucial.

For all search channels previously mentioned a precise prediction of the
signal cross section and of the kinematic properties of the produced
final-state particles is of utmost importance, together with a possibly
accurate estimation of the connected systematic uncertainties.  The
scope of this section is to present the state-of-the-art inclusive cross
sections for the $\PW\PH$ and $\PZ\PH$ Higgs-boson production modes at different
LHC centre-of-mass energies and for different possible values of the
Higgs-boson mass and their connected uncertainties.
%in case we will have this...
%Some first preliminary results 
%of differential properties of the Higgs boson in these production modes and their connected uncertainties 
%will be presented as well.

\subsection{Theoretical framework}

The inclusive partonic cross section for associated production of a
Higgs boson ($\PH$) and a weak gauge boson ($V$) can be written as
\begin{equation}
\begin{split}
\hat \sigma(\hat s) = \int_0^{\hat s} {\rm d}k^2
\sigma(V^\ast(k))\,\frac{{\rm d}\Gamma}{{\rm d}k^2} (V^\ast(k)\to \PH V) +
\Delta\sigma\,,
\label{eq:sigpart}
\end{split}
\end{equation}
where $\sqrt{\hat s}$ is the partonic centre-of-mass energy.
The first term on the r.h.s.\ arises from terms where a virtual gauge
boson $V^\ast$ with momentum $k$ is produced in a Drell--Yan-like
process, which then radiates a Higgs boson. The factor $\sigma(V^\ast)$
is the total cross section for producing the intermediate vector boson
and is fully analogous to the Drell--Yan expression. The second term on
the r.h.s., $\Delta\sigma$, comprises all remaining contributions.
The hadronic cross section is obtained from the partonic expression of
Eq.\,(\ref{eq:sigpart}) by convolving it with the parton densities in
the usual way.

The LO prediction for $pp\to V\PH$ ($V=\PW,\PZ$) is based on the Feynman
diagrams shown in \Fref{fig:ppVH-LO-diags}\,(a),(b) and leads to a
LO cross section of ${\cal O}(\GF^2)$.
\begin{figure}
\begin{center}
\begin{tabular}{ccc}
{\unitlength 1pt \SetScale{1}
\begin{picture}(150,100)(-5,-10)
\ArrowLine(20, 5)(50,40)
\ArrowLine(50,40)(20,75)
\Photon(50,40)(90,40){2}{5}
\Photon(120,20)(90,40){2}{5}
\DashLine(90,40)(120,60){5}
\Vertex(50,40){2}
\Vertex(90,40){2}
\put( -4, 2){${\PQu/\PQd}$}
\put( -4,72){$\PAQd/\PAQu$}
\put(125,56){${\PH}$}
\put( 62,25){${\PW}$}
\put(125,12){${\PW^\pm}$}
\end{picture}
}\hspace*{-2em} &
{\unitlength 1pt \SetScale{1}
\begin{picture}(150,100)(-5,-10)
\ArrowLine(20, 5)(50,40)
\ArrowLine(50,40)(20,75)
\Photon(50,40)(90,40){2}{5}
\Photon(120,20)(90,40){2}{5}
\DashLine(90,40)(120,60){5}
\Vertex(50,40){2}
\Vertex(90,40){2}
\put(  8, 2){${\PQq}$}
\put(  8,72){${\PAQq}$}
\put(125,56){${\PH}$}
\put( 65,25){${\PZ}$}
\put(125,12){${\PZ}$}
\end{picture}
}\hspace*{-2em} &
{\unitlength 1pt \SetScale{1}
\begin{picture}(150,100)(-5,-10)
\Gluon(50,15)(20, 5){4}{3}
\Gluon(20,75)(50,65){4}{3}
\ArrowLine(50,65)(50,15)
\ArrowLine(90,65)(50,65)
\ArrowLine(50,15)(90,15)
\ArrowLine(90,15)(90,65)
\Photon(120,10)(90,15){2}{5}
\DashLine(90,65)(120,70){5}
\Vertex(90,65){2}
\Vertex(90,15){2}
\Vertex(50,15){2}
\Vertex(50,65){2}
\put(  55, 30){$\PQt$}
\put(  8, 2){$\Pg$}
\put(  8,72){$\Pg$}
\put(125,68){$\PH$}
\put(125,7){$\PZ$}
\end{picture}
}\\
(a) & (b) & (c)
\end{tabular}
\end{center}
\vspace*{-1em}
\caption{(a), (b) LO diagrams for the partonic processes $\Pp\Pp\to V\PH$
  ($V=\PW,\PZ$); (c) diagram contributing to the $\Pg\Pg\to \PH\PZ$ channel.}
\label{fig:ppVH-LO-diags}
\end{figure}
Through NLO, the QCD
corrections are fully given by the NLO QCD corrections to the Drell-Yan
cross section $\hat
\sigma(V^\ast)$~\cite{Han:1991ia,Baer:1992vx,Ohnemus:1992bd}. For $V=\PW$,
this observation carries over to NNLO%
\footnote{This statement holds up to two-loop diagrams where the Higgs
  boson is attached to a one-loop Drell--Yan diagram via the loop-induced
  $\Pg\Pg\PH$ coupling. Such diagrams, which are neglected so far, are believed 
  to have only a small impact; their calculation is in progress.},
so that the corresponding QCD
corrections can be easily derived by integrating the classic Drell--Yan
result~\cite{Hamberg:1990np,Harlander:2002wh} over the virtuality of the
intermediate gauge boson. For that purpose, the program {\tt vh@nnlo}
has been developed~\cite{Brein:2003wg}, building on the publically
available code {\tt zwprod.f}~\cite{Hamberg:1990np}.

The Drell--Yan-like corrections that determine the NNLO result for
$\PW\PH$ production also give the bulk of the $\PZ\PH$
contribution. However, in that case, there are gluon--gluon-induced
terms that do not involve a virtual weak gauge boson; both the $\PZ$ and
the $\PH$ couple to the gluons via a top-quark loop in this case, see
\Fref{fig:ppVH-LO-diags}\,(c). This class of diagrams is not taken
into account in {\tt vh@nnlo}; it was computed in
\Bref{Brein:2003wg}, and the numbers included in the results below are based on
the corresponding numerical code.

As every hadron collider observable that is evaluated at fixed order
perturbation theory, the cross section depends on the unphysical
renormalization and factorization scales $\mu_R$ and $\mu_F$.  Since the
QCD corrections mostly affect the production of the intermediate gauge
boson, a natural choice for the central value of $\mu_F$ and $\mu_R$ is
the virtuality $k^2$ of this gauge boson.

NLO electroweak (EW) corrections have been evaluated in
\Bref{Ciccolini:2003jy}.  In contrast to the NLO QCD corrections,
EW corrections do not respect a factorization into
Drell--Yan-like production and decay, since there are irreducible (box)
corrections to $\PQq\PQq^{(\prime)}\to V\PH$ already at one loop.  Note also
that the size of the EW corrections (as usual) sensitively
depend on the chosen renormalization scheme to define the weak
couplings, most notably on the choice for the electromagnetic couplings
$\alpha$. The preferred choice, which should be most robust with
respect to higher-order corrections, is the so-called $\GF$ scheme,
where $\alpha$ is derived from Fermi's constant $\GF$.

The combination of QCD and EW corrections poses
the question on whether factorization of the
EW and QCD effects is a valid approximation to the actual
mixed ${\cal O}(\GF\alphas)$ corrections. Following \Bref{Brein:2004ue},
we present our result based
on the assumption that full factorization of the two effects is valid,
i.e., the cross section is determined as
\begin{equation}
\begin{split}
\sigma = \sigma_{\rm NNLO}\times (1 + \delta_{\rm EW})\,,
\end{split}
\end{equation}
where $\sigma_{\rm NNLO}$ is the QCD result through ${\cal
  O}(\alphas^2)$, and $\delta_{\rm EW}$ is the relative EW correction
factor determined in the limit $\alphas=0$.

The PDF+$\alphas$ uncertainties are evaluated according to the recipe
proposed in Section~\ref{sec:pdf4lhcreco} of this report. The
uncertainties due to the residual dependence on the regularization and
factorization scales are determined by considering the cross section
when mutually fixing one of $\mu_R$ and $\mu_F$ at the central scale
$k^2$ (the mass of the intermediate gauge boson, see above), and varying
the other scale between $\sqrt{k^2}/3$ and $3\sqrt{k^2}$.
The EW factor $\delta_{\rm EW}$ is always calculated in the same way as
the central value of the cross section, because the relative EW correction
is insensitive to the PDF and/or scale choices.

In principle there are also real NLO EW corrections induced by
initial-state photons, which are ignored, since current PDF sets do not
deliver a photon PDF.  The photon PDF is, however, strongly suppressed,
so that an uncertainty of not more than 1\% should arise from this
approximation.  This estimated per-cent uncertainty, which rests on the
comparison with other cross sections such as vector-boson
fusion~\cite{Ciccolini:2007ec,Ciccolini:2007jr} where these effects have
been calculated, also includes the neglect of NLO EW corrections in the
evolution of current PDFs.

\subsection{Numerical results}

The results for the NLO and the NNLO QCD cross sections for $\PW\PH$
production, including NLO EW effects, are shown in
\Fref{fig:wh-xsec}, both at 7 and 14\UTeV. The numbers are obtained
by summing over $\PWp\PH$ and $\PWm\PH$ production.
The corresponding K-factors, obtained by normalizing the cross section
to the LO value (at central scales and PDFs), are shown in
\Fref{fig:wh-k}. The little kinks at around 160\UGeV\ and, somewhat
smaller, 180\UGeV\ are due to the $\PW\PW$ and $\PZ\PZ$ thresholds that occur
in the EW radiative corrections (see also \Bref{Ciccolini:2003jy}).
The present prediction does not properly describe the threshold
behaviour, which is in fact singular on threshold. Therefore, 
in practice, Higgs mass windows of $\sim \pm 5\UGeV$ around the 
thresholds should be obtained upon
interpolation unless the threshold regions are properly described
(e.g.\ by complex masses), a task which is in progress.
The uncertainty of the threshold interpolation is about 1\%.

The plots for $\PZ\PH$ production are shown in \Frefs{fig:zh-xsec}
and \ref{fig:zh-k}. The fact that the uncertainty bands at NNLO are of
the same order of magnitude as at NLO is due to the the $\Pg\Pg$ channel
that occurs only at NNLO and is absent in the $\PW\PH$ case.

We have checked the NLO numbers against {\tt v2hv}~\cite{V2HV} and find
agreement at the per-mille level, once CKM mixing is included in {\tt
  v2hv}. Also, we find satisfactory agreement of the NLO result when
comparing to MCFM~\cite{MCFM}. However, the comparison is less strict in
this case as MCFM does not allow the same scale choice as done here.

The results for the central values of the cross section and the
corresponding theoretical uncertainties are shown in
\Tref{tab:wzh7} and \ref{tab:wzh14} for 7 and 14\UTeV,
respectively. Notice that the scale uncertainties for $\PZ\PH$ production
are consistently larger than for $\PW\PH$ production, because they are
dominated by the uncertainties of the $\Pg\Pg$ channel.

\begin{figure}
\vspace{0pt}
\begin{center}
\begin{tabular}{cc}
\includegraphics[bb=20 270 550
  820,angle=90,width=.46\linewidth]{YRHXS_WHZH/wh-xsec-nlo.ps} &
\includegraphics[bb=20 270 550
  820,angle=90,width=.46\linewidth]{YRHXS_WHZH/wh-xsec-nnlo.ps}\\[-1.5em] 
(a) & (b)
\end{tabular}
\end{center}
\vspace*{-1em}
\caption[]{\label{fig:wh-xsec} Cross section for the sum of $\PWp\PH$
  and $\PWm\PH$ production for 7 and 14\UTeV\ at (a) NLO and (b) NNLO
  QCD, including NLO EW effects in both cases.}
%\end{figure}
%
%\begin{figure}
\vspace{0pt}
\begin{center}
\begin{tabular}{cc}
\includegraphics[bb=20 270 550
  820,angle=90,width=.46\linewidth]{YRHXS_WHZH/wh7-k-nlo-lo.ps} &
\includegraphics[bb=20 270 550
  820,angle=90,width=.46\linewidth]{YRHXS_WHZH/wh7-k-nnlo-lo.ps}\\[-1.5em] 
(a) & (b)\\[.5em]
\includegraphics[bb=20 270 550
  820,angle=90,width=.46\linewidth]{YRHXS_WHZH/wh14-k-nlo-lo.ps} &
\includegraphics[bb=20 270 550
  820,angle=90,width=.46\linewidth]{YRHXS_WHZH/wh14-k-nnlo-lo.ps}\\[-1.5em] 
(c) & (d)
\end{tabular}
\end{center}
\vspace*{-1em}
\caption[]{\label{fig:wh-k}K-factors (ratio to LO prediction) for the
  NLO and NNLO cross sections of \Fref{fig:wh-xsec}.}
\end{figure}

\begin{figure}
\vspace{0pt}
\begin{center}
\begin{tabular}{cc}
\includegraphics[bb=20 270 550
  820,angle=90,width=.46\linewidth]{YRHXS_WHZH/zh-xsec-nlo.ps} &
\includegraphics[bb=20 270 550
  820,angle=90,width=.46\linewidth]{YRHXS_WHZH/zh-xsec-nnlo.ps}\\[-1.5em]  
(a) & (b)
\end{tabular}
\end{center}
\vspace*{-1em}
\caption[]{\label{fig:zh-xsec} Cross section for $\PZ\PH$
  production for 7 and 14\UTeV\ at (a) NLO and (b) NNLO
  QCD, including NLO EW effects in both cases.}
%\end{figure}
%
%\begin{figure}
\vspace{0pt}
\begin{center}
\begin{tabular}{cc}
\includegraphics[bb=20 270 550
  820,angle=90,width=.46\linewidth]{YRHXS_WHZH/zh7-k-nlo-lo.ps} &
\includegraphics[bb=20 270 550
  820,angle=90,width=.46\linewidth]{YRHXS_WHZH/zh7-k-nnlo-lo.ps}\\[-1.5em]  
(a) & (b)\\[.5em]
\includegraphics[bb=20 270 550
  820,angle=90,width=.46\linewidth]{YRHXS_WHZH/zh14-k-nlo-lo.ps} &
\includegraphics[bb=20 270 550
  820,angle=90,width=.46\linewidth]{YRHXS_WHZH/zh14-k-nnlo-lo.ps}\\[-1.5em]  
(c) & (d)
\end{tabular}
\end{center}
\vspace*{-1em}
\caption[]{\label{fig:zh-k}K-factors (ratio to LO prediction) for the
  NLO and NNLO cross sections of \Fref{fig:zh-xsec}.}
\end{figure}

\begin{table}
\include{YRHXS_WHZH/wzh7nnlo}
\caption[]{\label{tab:wzh7}Total inclusive cross section at 7\UTeV\ for
  $\PW\PH$ and $\PZ\PH$ production at NNLO QCD + NLO EW. The first error
  indicates the uncertainty from the renormalization and factorization
  scale variation, the second from the PDF+$\alphas$ variation.}
\end{table}

\begin{table}
\include{YRHXS_WHZH/wzh14nnlo}
\caption[]{\label{tab:wzh14}Total inclusive cross section at
  14\UTeV\ for $\PW\PH$ and $\PZ\PH$ production at NNLO QCD + NLO
  EW. The first error indicates the uncertainty from the renormalization
  and factorization scale variation, the second from the PDF+$\alphas$
  variation.}
\end{table}

\clearpage


%% \bibliographystyle{atlasnote}
%% \bibliography{YRHXS_bib}

%% \end{document}
