\section{MSSM Charged Higgs Production Process\footnote{Martin Flechl,
    Michael Kr\"amer, Sami Lehti (eds.); Stefan Dittmaier, Thomas
    Hahn, Tuomo Hartonen, Sven Heinemeyer, Jae Sik Lee, Apostolos
    Pilaftsis, Michael Spira, Carole Weydert}}
\label{sec:chiggs_intro}

Many extensions of the Standard Model, in particular supersymmetric
theories, require two Higgs doublets leading to five physical scalar
Higgs bosons, including two (mass-degenerate) charged particles ${\rm
  H}^\pm$. The discovery of a charged Higgs boson would provide
unambiguous evidence for an extended Higgs sector beyond the Standard
Model. Searches at LEP have set a limit $M_{{\rm H}^\pm} > 79.3$~GeV
on the mass of a charged Higgs boson in a general two-Higgs-doublet
model~\cite{Heister:2002ev}. Within the MSSM, the charged Higgs boson
mass is constrained by the pseudoscalar Higgs mass and the W boson
mass through $M^2_{{\rm H}^\pm} = M^2_{{\rm A}} + M^2_{{\rm W}}$ at
tree level, with only moderate higher-order
corrections~\cite{Gunion:1988pc, Brignole:1991wp, Diaz:1991ki,
  Frank:2006yh}. A mass limit on the MSSM charged Higgs boson can thus
be derived from the limit on the pseudoscalar Higgs boson, $M_{{\rm
    A}} > 93.4$~GeV~\cite{Schael:2006cr}, resulting in $M_{{\rm
    H}^\pm}\gsim 120$~GeV. At the Tevatron, searches for light charged
Higgs bosons in top quark decays $\Pt \to \Pb {\rm
  H}^\pm$~\cite{Aaltonen:2009ke,:2009zh} have placed some constraints
on the MSSM parameter space, but do not provide any further generic
bounds on $M_{\rm H}^{\pm}$.

There are two main mechanisms for charged Higgs boson production at
the LHC:
\begin{displaymath}
\begin{array}{lcl}
  \mbox{top quark decay:} & \Pt \to \Pb {\rm H}^{\pm}+X &\; {\rm if}\;\;
  M_{{\rm H}^\pm} \lsim m_{\rm t}\,, \\
  \mbox{associate production:} & \Pp\Pp \to {\rm t}\Pb{\rm H}^{\pm}+X
  &\; {\rm if}\;\; M_{{\rm H}^\pm} \gsim m_{\rm t}\,.
\end{array}
\end{displaymath}
Alternative production mechanisms like quark--antiquark annihilation
$\Pq\bar \Pq'\to \PH^\pm$, $\PH^\pm+\mathrm{jet}$ production,
associated $\PH^\pm\PW^\mp$ production or Higgs pair production have
suppressed rates, and it is not yet clear whether a signal could be
established in any of those channels (see \Ref~\cite{Djouadi:2005gj}
and references therein). Some of the above production processes may,
however, be enhanced in models with non-minimal flavour violation (see
e.g. \Ref~\cite{Dittmaier:2007uw}).

In this section we discuss charged Higgs boson production in $\Pt \to
\Pb {\rm H}^\pm$ decays and compare the results of different software
packages for the calculation of this branching ratio. Furthermore, we
present NLO QCD predictions for the process $\Pp\Pp \to {\rm t
  bH}^\pm+X$ in the so-called four- and five flavour schemes.


\subsection{Light charged Higgs production from top quark decays}

If the charged Higgs boson is light, $m_{{\rm H}^{\pm}} \lsim m_{\rm
  t}$, it is produced in top quark decays. The branching ratio
calculation of the top quark to a light charged Higgs boson is
compared for two different programs, {\sl FeynHiggs}, version
2.7.3~\cite{Heinemeyer:1998yj,Heinemeyer:1998np,Degrassi:2002fi,Frank:2006yh}
and {\sl CPsuperH}, version 2.2~\cite{Lee:2003nta,Lee:2007gn}. The
$m_h^{\rm max}$ benchmark scenario was used according to the
definition in \Ref~\cite{Carena:2002qg}:
\begin{eqnarray}
  m_h^{\rm max}: & M_{\rm SUSY} = 1000~{\rm GeV}, 
  X_{\rm t} = 2~M_{\rm SUSY}, A_{\rm b} = A_{\rm t},\\
  & \mu = 200~{\rm GeV}, M_{2} = 200~{\rm GeV}, 
  m_{\tilde{g}} = 0.8~M_{\rm SUSY}\,.\nonumber
\end{eqnarray}
In addition to $\tan \beta$ and $M_{\rm H^{\pm}}$, the $\mu$ parameter
was varied with values $\pm 1000, \pm 200$~GeV.  The standard model
parameters were taken as given in the Appendix
\Table~\ref{tab:SMinput}.

The calculation within {\sl FeynHiggs} is based on the evaluations of
$\Gamma(\rm t \to W^+ b)$ and $\Gamma(\rm t \to H^+ b)$. The former is
calculated at NLO according to \Ref~\cite{Campbell:2004ch}. The decay
to the charged Higgs boson and the bottom quark uses $m_{\rm t}(m_{\rm
  t})$ and $m_{\rm b}(m_{\rm t})$ in the Yukawa coupling, where the
latter receives the additional correction factor $1/(1 + \Delta_b)$,
evaluated following \Ref~\cite{Hofer:2009xb}.  Furthermore additional
QCD corrections taken from \Ref~\cite{Carena:1999py} are included.

The calculation within {\sl CPsuperH} is also based on the top-quark
decays $\rm t \to W^+ b$ and $\rm t\to H^+ b$.  The decay width
$\Gamma(\rm t \to W^+ b)$ is calculated by including ${\cal
  O}(\alpha_{\rm s})$ corrections~\cite{Chetyrkin:1999br}.
%
The partial decay width of the decay $\rm t \to H^+ b$ is given by
\begin{eqnarray}
\Gamma(\rm t\rightarrow H^+ b)&=&\frac{g_{\rm tb}^2m_{\rm t}}{16\pi}
\left(|g^S_{_{{\rm H}^+\bar{\rm t}{\rm b}}}|^2+|g^P_{_{{\rm
        H}^+\bar{\rm t}{\rm b}}}|^2\right)
\left(1-\frac{m_{{\rm H}^\pm}^2}{m_{\rm t}^2}\right)^2\,,
\end{eqnarray}
where $g_{\rm tb} = - g m_{\rm t}/\sqrt{2} M_{\rm W}$, $g^S_{_{{\rm
      H}^+\bar{\rm t}{\rm b}}}=(g^L_{_{{\rm H}^+\bar{\rm t}{\rm b}}} +
\frac{m_{\rm b}}{m_{\rm t}}\, g^R_{_{{\rm H}^+\bar{\rm t}{\rm
      b}}})/2$, and $g^P_{_{{\rm H}^+\bar{\rm t}{\rm
      b}}}=i(g^L_{_{{\rm H}^+\bar{\rm t}{\rm b}}} - \frac{m_{\rm
    b}}{m_{\rm t}}\, g^R_{_{{\rm H}^+\bar{\rm t}{\rm b}}})/2$.  In the
couplings $g^{L,R}_{_{{\rm H}^+\bar{\rm t}{\rm b}}}$, all the
threshold corrections (both those enhanced and not enhanced by
$\tan\beta$) have been included as described in Appendix A of
\Ref~\cite{Lee:2003nta} and \Refs~\cite{Carena:2002bb,Ellis:2009di}.
%
For $m_{\rm t}$ and $m_{\rm b}$ appearing in the couplings, we use the
quark masses evaluated at the scale $M_{\rm H^\pm}$.

The comparison started by running {\sl FeynHiggs} with a selected set
of parameters. The {\sl FeynHiggs} output was used to set the values
for the CPsuperH input parameters. Due to differences in the parameter
definitions, the bottom quark mass was changed from {\sl FeynHiggs}
$m_{\rm b}(m_{\rm b}) = 4.16~{\rm GeV}$ to $m_{\rm b}(m_{\rm t}) =
2.64~{\rm GeV}$ which {\sl CPsuperH} takes as input.  The main result
from the comparison is shown in Figs.~\ref{fig:BRTop2HPlus} and
\ref{fig:BRTop2HPlusDiff}.

\begin{figure}[ht]
  \centering
\begin{tabular}{cc}
  \includegraphics[width=75mm]{YRHXS_MSSM_charged/YRHXS_MSSM_charged_fig1.eps} &
  \includegraphics[width=75mm]{YRHXS_MSSM_charged/YRHXS_MSSM_charged_fig2.eps} \\
  \includegraphics[width=75mm]{YRHXS_MSSM_charged/YRHXS_MSSM_charged_fig3.eps} &
  \includegraphics[width=75mm]{YRHXS_MSSM_charged/YRHXS_MSSM_charged_fig4.eps} \\
\end{tabular}
\caption{The branching fraction of $\rm t\rightarrow bH^{\pm}$ as a
  function of tan$\beta$ for different values of mu and $ M_{{\rm
      H}^{\pm}}$. The solid black curves are calculated with {\sl
    FeynHiggs}, and the dotted green curves with {\sl CPsuperH}.}
\label{fig:BRTop2HPlus}
\end{figure}

\begin{figure}[ht]
  \centering
\begin{tabular}{cc}
  \includegraphics[width=75mm]{YRHXS_MSSM_charged/YRHXS_MSSM_charged_fig5.eps} &
  \includegraphics[width=75mm]{YRHXS_MSSM_charged/YRHXS_MSSM_charged_fig6.eps} \\
  \includegraphics[width=75mm]{YRHXS_MSSM_charged/YRHXS_MSSM_charged_fig7.eps} &
  \includegraphics[width=75mm]{YRHXS_MSSM_charged/YRHXS_MSSM_charged_fig8.eps} \\
\end{tabular}
\caption{The difference of BR($\rm t\rightarrow bH^{\pm}$) calculated
  with {\sl CPsuperH} and {\sl FeynHiggs} as a function of tan$\beta$
  for different values of $\mu$ and $m_{{\rm H}^{\pm}}$.}
\label{fig:BRTop2HPlusDiff}
\end{figure}

\subsection{Heavy charged Higgs production with top and bottom quarks}
\label{subsec:CH_tbh}

Two different formalisms can be employed to calculate the cross
section for associated ${\rm t}\Pb{\rm H}^\pm$ production.  In the
four-flavour scheme (4FS) with no $\Pb$ quarks in the initial state,
the lowest-order QCD production processes are gluon--gluon fusion and
quark--antiquark annihilation, $\Pg\Pg \to {\rm t}\Pb{\rm H}^\pm$ and
$\Pq\bar \Pq \to {\rm t}\Pb{\rm H}^\pm$, respectively. Potentially
large logarithms $\propto \ln(\mu_{\rm F}/m_\Pb)$, which arise from
the splitting of incoming gluons into nearly collinear $\Pb\bar \Pb$
pairs, can be summed to all orders in perturbation theory by
introducing bottom parton densities. This defines the five-flavour
scheme (5FS)~\cite{Barnett:1987jw}. The use of bottom distribution
functions is based on the approximation that the outgoing $\Pb$ quark
is at small transverse momentum and massless, and the virtual $\Pb$
quark is quasi on-shell. In this scheme, the leading-order (LO)
process for the inclusive ${\rm tbH}^\pm$ cross section is
gluon--bottom fusion, $\Pg \Pb \to {\rm tH}^\pm$.  The next-to-leading
order (NLO) cross section in the 5FS includes ${\cal
  O}(\alpha_{\mathrm{s}})$ corrections to $\Pg \Pb \to {\rm tH}^\pm$
and the tree-level processes $\Pg\Pg \to {\rm tbH}^\pm$ and $\Pq\bar
\Pq \to {\rm tbH}^\pm$. To all orders in perturbation theory the four-
and five-flavour schemes are identical, but the way of ordering the
perturbative expansion is different, and the results do not match
exactly at finite order. For the inclusive production of neutral Higgs
bosons with bottom quarks, $\Pp\Pp \to \Pb\bar{\Pb}{\rm H}+X$, the
four- and five-flavour scheme calculations numerically agree within
their respective uncertainties, once higher-order QCD corrections are
taken into account~\cite{Dittmaier:2003ej, Campbell:2004pu,
  Dawson:2005vi, Buttar:2006zd}, see \Section~6 of this report.
  
There has been considerable progress recently in improving the cross
section predictions for the associated production of charged Higgs
bosons with heavy quarks by calculating NLO SUSY-QCD and electroweak
corrections in the four and five-flavour schemes~\cite{Zhu:2001nt,
  Gao:2002is, Plehn:2002vy, Berger:2003sm, Kidonakis:2005hc,
  Peng:2006wv, Beccaria:2009my, Kidonakis:2010ux}, and the matching of
the NLO five-flavour scheme calculation with parton
showers~\cite{Weydert:2009vr}. Below, we shall present
state-of-the-art NLO QCD predictions in the 4FS
(\Section~\ref{subsubsec:CH_4FS}), in the 5FS
(\Section~\ref{subsubsec:CH_5FS}), and a first comparison of the two
schemes at NLO (\Section~\ref{subsubsec:CH_4and5FS}).

\subsubsection{NLO QCD predictions for $p p \to tbH^{\pm}+X$ in the 4FS}
\label{subsubsec:CH_4FS}

In the 4FS the production of charged Higgs bosons in association with
top and bottom quarks proceeds at LO through the parton processes
$\Pg\Pg \to {\rm t}\bar{\Pb}{\rm H}^-$ and $\Pq\bar{\Pq} \to {\rm
  t}\bar{\Pb}{\rm H}^-$, and the charge-conjugate processes with the
$\bar{\rm t}{\rm bH}^+$ final state~\cite{DiazCruz:1992gg,
  Borzumati:1999th, Miller:1999bm}. Throughout this section we present
results for the ${\rm t}\bar{\Pb} {\rm H}^-$ channels. Generic Feynman
diagrams that contribute at LO are displayed in Fig.~\ref{fig:diags}.
\begin{figure}
\begin{center}
\epsfig{file=YRHXS_MSSM_charged/feyn.ps,%
%  bbllx=55pt,bblly=625pt,bburx=596pt,bbury=842pt,%
  bbllx=55pt,bblly=625pt,bburx=446pt,bbury=842pt,%
        clip=,scale=0.8}
      \caption{Generic Feynman diagrams for $\Pp\Pp \to {\rm t}\Pb{\rm
          H}^{\pm}+X$ in the 4FS at the Born level.}
\label{fig:diags}
\end{center}
\end{figure}
% ---------------------------------------------------------------------

The calculation of the NLO QCD corrections to charged Higgs production
in the 4FS has been discussed in detail in
\Ref~\cite{Dittmaier:2009np}, both within a two-Higgs-doublet model
with the SM particle content besides the extended Higgs sector, and
within the MSSM. The NLO QCD effects considerably enhance the cross
section and reduce the dependence on the renormalization and
factorization scales. In the MSSM, additional loop-corrections from
squark and gluino exchange are sizable for large $\tan\beta$, but they
can be taken into account through the $\Delta_{\Pb}$-corrections to
the bottom-Higgs-Yukawa coupling, {\it i.e.} through a rescaling of
the NLO-QCD prediction according to $\Mb\tan\beta/v \to \Mb\tan\beta/v
\, (1 - \Delta_{\Pb}/\tan^2\beta) / (1 +
\Delta_{\Pb})$~\cite{Dittmaier:2009np}.

In \Tables~\ref{tab::charged_higgs_4fs_nlo_7tev} and
\ref{tab::charged_higgs_4fs_nlo_14tev} we present 4FS NLO QCD results
for the production of heavy charged Higgs bosons in a
two-Higgs-doublet model. Cross sections for MSSM scenarios with large
$\tan\beta$ can be obtained from the NLO-QCD cross sections by the
rescaling defined above. Predictions are presented for LHC cross
sections at 7 and 14~TeV energy, with $\tan\beta = 30$ and the SM
input parameters according to \Table~\ref{tab:SMinput}.
%
\begin{table}[h!]
  \caption{\label{tab::charged_higgs_4fs_nlo_7tev}  
    NLO QCD cross sections for $\Pp\Pp \to {\rm
      t}\bar{\Pb}{\rm H}^-+X$ in the 4FS at the LHC with 7 TeV, $\tan \beta=30$.}
\centering
\begin{tabular}{ccccc}\hline
$\MH^\pm$~[GeV] & $\sigma$~[pb] & scale uncert. [\%] & PDF + \alphas\; [\%] & total [\%] \\
\hline
200 & 130 &  $-33, +27$  & $-5.5, +4.5$ & $-33, +27$\\
300 & 45.9 &  $-33, +34$  & $-6.7, +5.6$ & $-34, +34$\\
400 & 18.0 &  $-34, +30$  & $-7.7, +6.6$ & $-35, +31$\\
500 & 7.59 &  $-35, +32$  & $-8.6, +7.5$ & $-36, +33$\\
\hline
\end{tabular}
\end{table}
%
\begin{table}[h!]
  \caption{\label{tab::charged_higgs_4fs_nlo_14tev}  
    NLO QCD cross sections for $\Pp\Pp \to {\rm
      t}\bar{\Pb}{\rm H}^-+X$ in the 4FS at the LHC with 14 TeV, $\tan \beta=30$.}
\centering
\begin{tabular}{ccccc}\hline
$\MH^\pm$~[GeV] & $\sigma$~[pb] & scale uncert. [\%] & PDF + \alphas\; [\%] & total [\%] \\
\hline
200 & 972 &  $-30, +27$  & $-3.4, +2.7$ & $-30, +27$\\
300 & 405 &  $-30, +26$  & $-4.0, +3.2$ & $-30, +26$\\
400 & 184 &  $-30, +26$  & $-4.7, +3.7$ & $-30, +26$\\
500 & 92.6 &  $-32, +29$  & $-5.1, +4.1$ & $-32, +29$\\
\hline
\end{tabular}
\end{table}
%
For a consistent evaluation of the hadronic cross sections in the 4FS
we adopt the recent MSTW four-flavour pdf~\cite{Martin:2010db} and the
corresponding four-flavour $\alpha_{\rm s}$.  Note, however, that the
evaluation of the running \Pb-quark mass in the bottom-Higgs-Yukawa
coupling is based on a five-flavour $\alpha_{\rm s}$ with $\alpha_{\rm
  s}(M_Z) = 0.120$. The renormalization and factorization scales have
been identified and are set to $\mu = (m_{\rm t} + m_{\Pb} + M_{{\rm
    H}^-})/3$ as our default choice. The NLO scale uncertainty has
been estimated from the variation of the renormalization and
factorization scales by a factor of three about the central scale
choice $\mu = (m_{\rm t} + m_{\Pb} + M_{{\rm
    H}^-})/3$~\cite{Dittmaier:2009np}. The residual NLO scale
uncertainty is approximately $\pm 30$\%. While no four-flavour pdf
parametrization exists that would allow to estimate the combined pdf
and $\alpha_{\rm s}$ error, the difference in the relative pdf error
as obtained from the MSTW four- and five-flavour sets is marginal.  We
have thus adopted the five-flavour MSTW pdf~\cite{Martin:2009iq} to
estimate the combined pdf and $\alpha_{\rm s}$ uncertainty shown in
\Tables~\ref{tab::charged_higgs_4fs_nlo_7tev} and
\ref{tab::charged_higgs_4fs_nlo_14tev}. We find that the theoretical
uncertainty of the 4FS NLO-QCD prediction for $\Pp\Pp \to {\rm
  t}\bar{\Pb}{\rm H}^-+X$ at the LHC is by far dominated by the scale
uncertainty.

The NLO-QCD cross section for $\Pp\Pp \to {\rm t}\bar{\Pb}{\rm
  H}^-+X$ at the LHC with 7 and 14 TeV is shown in
\Figure~\ref{fig:totalxs} as a function of the Higgs-boson mass. The
error band includes the NLO scale uncertainty and the pdf+$\alpha_{\rm
  s}$ error, added in quadrature.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}
\begin{center}
% \includegraphics[bb = 170 530 500 820, %width=0.48\textwidth]{YRHXS_MSSM_charged/totalxs_7TeV.eps}
% \includegraphics[bb = 170 530 500 820, %width=0.48\textwidth]{YRHXS_MSSM_charged/totalxs_14TeV.eps}
\includegraphics[width=0.48\textwidth]{YRHXS_MSSM_charged/totalxs_7TeV.eps}
\includegraphics[width=0.48\textwidth]{YRHXS_MSSM_charged/totalxs_14TeV.eps}

 \caption{NLO QCD cross sections for $\Pp\Pp \to {\rm t}\bar{\Pb}{\rm
     H}^-+X$ in the 4FS at the LHC (7 TeV and 14 TeV) as a function of
   the Higgs-boson mass. The error band includes the NLO scale
   uncertainty and the pdf+$\alpha_{\rm s}$ error, added in
   quadrature. Calculation from \Ref~\cite{Dittmaier:2009np}.}
\label{fig:totalxs}
\end{center}
\end{figure}
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{NLO QCD predictions for $pp \to t H^{\pm}+X$ in the 5FS}
\label{subsubsec:CH_5FS}

In the 5FS the LO process for the inclusive ${\rm tH}^\pm$ cross
section is gluon-bottom fusion, $\Pg \Pb \to {\rm tH}^\pm$.  The NLO
cross section includes ${\cal O}(\alpha_{\mathrm{s}})$ corrections to
$\Pg \Pb \to {\rm tH}^\pm$ and the tree-level processes $\Pg\Pg \to
{\rm tbH}^\pm$ and $\Pq\bar \Pq \to {\rm tbH}^\pm$, and has been
calculated in \Refs~\cite{Plehn:2002vy, Berger:2003sm,
  Weydert:2009vr}. In \Tables~\ref{tab::charged_higgs_5fs_nlo_7tev}
and \ref{tab::charged_higgs_5fs_nlo_14tev} we present NLO QCD results
for the production of heavy charged Higgs bosons in the 5FS, with
$\tan\beta = 30$ and the SM input parameters according to
\Table~\ref{tab:SMinput}.  As in the 4FS calculation, cross sections
for MSSM scenarios with large $\tan\beta$ can be obtained from the
NLO-QCD cross sections by rescaling the bottom-Higgs-Yukawa coupling.
%
\begin{table}[h!]
  \caption{\label{tab::charged_higgs_5fs_nlo_7tev}  
    NLO QCD cross sections for $\Pp\Pp \to {\rm
      t}\bar{\Pb}{\rm H}^-+X$ in the 5FS at the LHC with 7 TeV, $\tan \beta=30$.}
  \centering
\begin{tabular}{ccccc}\hline
$\MH^\pm$~[GeV] & $\sigma$~[pb] & scale uncert. [\%] \\
\hline
200 & 178 &  $-7.1, +9.4$  \\
300 & 62.7 &  $-10, +4.7$  \\
400 & 24.7 &  $-11, +2.7$  \\
500 & 10.5 &  $-12, +1.1$  \\
\hline
\end{tabular}
\end{table}
%
\begin{table}[h!]
  \caption{\label{tab::charged_higgs_5fs_nlo_14tev}  
    NLO QCD cross sections for $\Pp\Pp \to {\rm
      t}\bar{\Pb}{\rm H}^-+X$ in the 5FS at the LHC with 14 TeV, $\tan \beta=30$.}
\centering
\begin{tabular}{ccccc}\hline
$\MH^\pm$~[GeV] & $\sigma$~[pb] & scale uncert. [\%] \\
\hline
200 & 1237 &  $-8.4, +13$  \\
300 & 521 &  $-9.0, +9.5$  \\
400 & 242 &  $-9.8, +7.7$  \\
500 & 121 &  $-10, +6.5$ \\
\hline
\end{tabular}
\end{table}
The NLO cross section values have been obtained using {\sc MC@NLO}
version 4, with the option \textit{rflag} switched to 0 (for MSbar
Yukawa renormalization). The central scale has been set to
$\mu_0=(m_t+m_{H^{-}})/4$, and the five-flavour MSTW
pdf~\cite{Martin:2009iq} has been adopted. We find a residual NLO
scale uncertainty of $10-20\%$. Since there are no direct experimental
constraints on the bottom-pdf, the pdf uncertainty of the $\Pg \Pb \to
{\rm tH}^\pm$ process is difficult to quantify. Thus, unfortunately,
no reliable estimates of the pdf and $\alpha_{\rm s}$ uncertainty of
the 5FS calculation exist to date.

The total 5FS NLO QCD cross section for $\Pp\Pp \to {\rm
  t}\bar{\Pb}{\rm H}^-+X$ at the LHC with 7 and 14 TeV is shown in
\Figure~\ref{fig:totalxs_5fs} as a function of the Higgs-boson mass.
The error band includes the NLO scale uncertainty only.

\begin{figure}
\begin{center}
%\includegraphics[width=0.48\textwidth]{YRHXS_MSSM_charged/xs7TeV_5fs.eps}
%\includegraphics[width=0.48\textwidth]{YRHXS_MSSM_charged/xs14TeV_5fs.eps}
\includegraphics[width=0.48\textwidth]{YRHXS_MSSM_charged/totalxs_7TeV_5FS.eps}
\includegraphics[width=0.48\textwidth]{YRHXS_MSSM_charged/totalxs_14TeV_5FS.eps}
\caption{NLO QCD cross sections for $\Pp\Pp \to {\rm t}\bar{\Pb}{\rm
    H}^-+X$ in the 5FS at the LHC (7 TeV and 14 TeV) as a function of
  the Higgs-boson mass. The error band includes the NLO scale
  uncertainty. Calculation from \Ref~\cite{Weydert:2009vr}.}
\label{fig:totalxs_5fs}
\end{center}
\end{figure}
        
Note that supersymmetric electroweak ${\cal O(\alpha)}$ corrections to
charged Higgs-boson production in the five-flavour scheme have been
studied in \Ref~\cite{Beccaria:2009my}. These corrections depend
sensitively on the MSSM scenario and have thus not been included in
the numbers presented here.

\subsubsection{Comparison of the 4FS and 5FS calculations}
\label{subsubsec:CH_4and5FS}

The 4FS and 5FS calculations represent different ways of ordering the
perturbative expansion, and the results will not match exactly at
finite order. However, taking into account higher-order corrections,
the two predictions are expected to agree within their respective
uncertainties, see \Section~6 of this report for a similar comparison
for the inclusive production of neutral Higgs bosons with bottom
quarks.

In \Figure~\ref{fig:4fs_5fs} we present a comparison of the 4FS and
5FS calculations at NLO QCD for the inclusive $\Pp \Pp \to {\rm
  tH}^{-}+X$ cross section at the LHC.
%
\begin{figure}
\begin{center}
\includegraphics[width=0.48\textwidth]{YRHXS_MSSM_charged/totalxs_7TeV_4vs5FS.eps}
\includegraphics[width=0.48\textwidth]{YRHXS_MSSM_charged/totalxs_14TeV_4vs5FS.eps}
%\includegraphics[bb = 170 530 500 820, %width=0.48\textwidth]{YRHXS_MSSM_charged/totalxs_7TeV_4vs5FS.eps}
%\includegraphics[bb = 170 530 500 820, %width=0.48\textwidth]{YRHXS_MSSM_charged/totalxs_14TeV_4vs5FS.eps}
\caption{NLO QCD cross sections for $\Pp\Pp \to {\rm t}\bar{\Pb}{\rm
    H}^-+X$ in the 4FS and 5FS at the LHC (7 TeV and 14 TeV) as a
  function of the Higgs-boson mass. The error band includes the NLO
  scale uncertainty. Calculations from \Refs~\cite{Dittmaier:2009np,
    Weydert:2009vr}.}
\label{fig:4fs_5fs}
\end{center}
\end{figure}
%        
The error band indicates the theoretical uncertainty when the
renormalization and factorization scales are varied between $\mu_0/3$
and $3\mu_0$. Taking the scale uncertainty into account, the 4FS and
5FS cross sections at NLO are consistent, even though the predictions
in the 5FS at our choice of the central scale are larger than those of
the 4FS by approximately 30\%, almost independently of the Higgs boson
mass. Qualitatively similar results have been obtained from a
comparison of 4FS and 5FS NLO calculations for single-top production
at the LHC~\cite{Campbell:2009ss}. Note that the bottom pdf of the
recent five-flavour MSTW fit~\cite{Martin:2009iq} is considerably
smaller than that of previous fits~\cite{Martin:2004ir} and has lead
to a significant decrease in the 5FS cross section prediction.

%\section*{Acknowledgments}
%This work is supported in part by the European Community's Marie-Curie
%Research Training Network under contract MRTN-CT-2006-035505 ``Tools
%and Precision Calculations for Physics Discoveries at Colliders'', the
%DFG SFB/TR9 ``Computational Particle Physics'', and the Helmholtz
%Alliance ``Physics at the Terascale''.

