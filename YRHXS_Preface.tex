\section{Preface}

After the start of $\Pp\Pp$ collisions at the LHC the natural question is: Why 
precision Higgs physics now? \\
The LHC successfully started at the end of 2009 colliding two proton 
beams at centre-of-mass energies of $\sqrt{s}= 0.9$  and $2.36$\UTeV. 
In 2010 the energy has been raised up to $7$\UTeV. By the end of the $7$\UTeV\ run, 
in 2011, each experiment aims to collect an integrated luminosity 
of $1$\ifb.
Then a long shutdown will allow to implement necessary modification to 
the machine, to restart again in 2013 at the designed energy of $14$\UTeV. 
By the end of the life of the LHC each experiment will have collected
$3000$\ifb on tape. 
The luminosity that the experiments expect to collect with the $7$\UTeV\ run 
will allow us to probe a wide range for the Higgs boson. Projections of 
ATLAS and CMS %\cite{atlasProj7tev,cmsProj7tev}, 
when combining only 
the three main channels ($\PH \rightarrow \PGg\PGg, \PH \rightarrow \PZ\PZ,\PH 
\rightarrow \PW\PW$), indicate that in case of no observed excess, the 
Standard Model (SM) Higgs boson can be excluded in the range between $140$ and 
$200$\UGeV.
A $5\,\sigma$ significance can be reached for a Higgs boson mass range 
between $160$\UGeV\ and $170$\UGeV. 
The experiments (ATLAS, CMS and LHCb) are now analyzing more channels 
in order to increase their potential for exclusion at lower and higher 
masses.
For these reasons an update of the discussion of the proper definition of 
the Higgs boson mass and width has become necessary.
Indeed, in this scenario, it is of utmost importance to access the best 
theory predictions for the Higgs cross sections and branching ratios,
using definitions of the Higgs boson properties that are objective 
functions of the experimental data while respecting first principles of 
Quantum Field Theory.
In all parts we have tried to give a widely homogeneous summary for the
precision observables. Comparisons among the various groups of authors
are documented reflecting the status of our theoretical knowledge.
This may be understood as providing a common opinion about the present
situation in the calculation of Higgs cross sections and their theoretical 
and parametric errors.

The experiments have a coherent plan for using the input suggestions
of the theoretical community to facilitate the combination of the 
individual results. Looking for precision tests of theoretical models 
at the level of their quantum structure, requires the highest standards on the 
theoretical side as well.
Therefore, the Report is the result of a workshop started as an appeal
by experimentalists. Its progress over the subsequent months to its 
final form was possible only because of a close contact between the 
experimental and theory communities. 

The major sections of this Report are devoted to discuss
computation of cross sections and branching ratios for the SM 
Higgs and for the Minimal Supersymmetric Standard Model (MSSM) Higgs bosons,
including the still-remaining theoretical uncertainties. 
The idea of presenting calculations on Higgs Physics was triggered by 
experimentalists and is substantiated as far as possible in this Report.
The Working Group has been organized in $10$ subgroups. The first four 
address different Higgs production modes: gluon--gluon fusion, 
vector-boson fusion, Higgs-strahlung and associated production with 
top-quark pairs. Two more groups are focusing on MSSM neutral and MSSM charged 
Higgs production. One group is dedicated to the measurement of the 
branching ratios (BR) of Higgs bosons in the
SM and MSSM. One group studies predictions from
different Monte Carlo (MC) codes at next-to-leading order (NLO) and  
their matching to parton-shower MCs. The definition of 
Higgs Pseudo-Observables is also a relevant part of this analysis, in 
order to correctly match the experimental observables and the 
theoretical definitions of physical quantities.
Finally one group is devoted to parton density functions (PDFs), looking
for new theoretical input and trying to pin down
the theoretical uncertainty on cross sections. 

To  exclude certain Higgs boson mass regions 
different inputs are needed:
%--
\begin{itemize}
\item SM cross sections and BR in order to produce predictions;
\item uncertainties on these quantities in order to quantify 
the theoretical uncertainty. These 
uncertainties enter also in the determination of systematic errors of the 
mean value.
\end{itemize}
%--

%--
Furthermore, common and correlated theoretical inputs 
(cross sections, PDFs, SM inputs, etc.) require the
highest standards on the theoretical side.
The goal has been to give precise common inputs to the experiments to 
facilitate the combination.

The structure of this Report centers on a description of cross sections 
computed at next-to-next-to-leading order (NNLO) or NLO, for each of the production 
modes. Comparisons among the various groups of authors for the central value
and the range of uncertainty are documented and reflect the 
status of our theoretical knowledge. Note that all the central values 
have been computed using the same SM input, as presented in table 
\refT{tab:SMinput} of the Appendix.
%--
An update of the previous discussions of theoretical uncertainties has 
become necessary for several reasons:
%--
\begin{itemize}
\item The PDF uncertainty has been computed following the PDF4LHC prescription 
as described in session \ref{pdfsection} of this report.
\item The $\alphas$ uncertainty has been added in quadrature to the 
PDF variation.
\item The renormalization and factorization QCD scales have been varied 
following the criterion of pinning down, as much as possible, the 
theoretical uncertainty. It often remains the largest of the uncertainties.
\end{itemize}
%--
A final major point is that, for this Report, all cross sections have
been computed within an inclusive setup, not taking into account the
experimental cuts and the acceptance of the apparatus. A dedicated study 
of these effects (cuts on the cross sections and on K-factors) will be 
presented in a future publication.

The final part of this Report is devoted to describe a new direction of 
work: what the experiments observe in the final state is not always 
directly connected to a well defined  theoretical quantity. 
We have to take into account the acceptance of the detector, the
definition of {\em signal}, the interference {\em signal--background} 
and all sort of approximations built in the Monte Carlo codes.
As an example at LEP, the line shape of the Z for the final state with 
two electrons has to be extracted from the cross section of the process 
($\Pep\Pem\rightarrow \Pep\Pem$), after having subtracted the contribution 
of the photon and the interference between the photon and the Z. 
A corrected definition of the Higgs mass and width is needed. Both are
connected to the corresponding complex pole in the $p^2$ plane
of the propagator with momentum transfer $p$. We claim that the correct 
definition of mass of an unstable particle has to be used in any Monte 
Carlo generator.

Different Monte Carlo generators exist at LO and NLO. It was important 
to compare their predictions and to stress the corresponding differences, 
also taking into account the different algorithms used for 
parton shower. 
Note that NLO matrix element generators matched with a parton shower 
are the tools for the future.
Beyond the goals of this Report remains the agreement between NLO MC 
predictions and NNLO calculations within the acceptance of the detectors.
The next step in the activities of this Working Group will be
the computation of cross sections that include acceptance cuts and differential
distributions for all final 
states that will be considered in the Higgs search at the LHC.
Preferably this should be carried out with the same set of (benchmark) cuts for
ATLAS and CMS. The goal is to understand how the K-factors from (N)LO 
to (N)NLO will change after introduction of cuts and to compare the NNLO 
differential distributions with the ones from Monte Carlo generators at 
NLO.
There is a final comment concerning the SM background: we plan to estimate
theoretical predictions for the most important backgrounds in the 
signal regions. This means that a {\em background control region} 
has to be defined and there the experiments will measure a given 
source of background, directly from data. 
The {\em control region} can be in the bulk of the background production 
phase space, but can also be in the tail of the distributions. Thus it 
is important to define the precision with which the SM background will be 
measured and the theoretical precision available for that particular 
region. Then the background uncertainty should be extrapolated back to 
the {\em signal region}, using available theoretical predictions and their 
uncertainty.
It will be important to compute  the interference between signal and 
background and try to access this at NLO.
The (N)LO Monte Carlos will be used to simulate this background and 
determine how the K-factor is changing with the chosen kinematic cuts.

The present documentation is the result of a Workshop that started in
January 2010 as a new joint effort for Higgs cross sections between ATLAS, CMS 
and the theory community.

We are obliged to Dippy Dog, Pluto and Donald Duck for their support and 
encouragement, to Elviry Coot for copy editing and to Huey, Dewey and 
Louie for organizational and technical assistance.  
We also acknowledge partial support from Beagle Boys.

\vspace{1.5cm}

\leftline{\it Stefan Dittmaier}
\leftline{\it Chiara Mariotti}
\leftline{\it Giampiero Passarino}
\leftline{\it Reisaburo Tanaka}
