LATEX    = latex
BIBTEX   = bibtex
DVIPS    = dvips

BASENAME = YRHXS

# Feynman diagrams using the feynMP
# Feynman diagrams mp file name
# feynMP0  = feyngraph0

default: testlatex

testlatex:
	latex  ${BASENAME}
	latex  ${BASENAME}
	bibtex ${BASENAME}
#	mpost ggh
#	mpost vbf
#	mpost whzh
#	mpost tth
#	mpost  ${feynMP0}
	latex  ${BASENAME}
	latex  ${BASENAME}
	dvips -o ${BASENAME}.ps ${BASENAME}.dvi  
	dvipdf -sPAPERSIZE=a4 -dPDFSETTINGS=/prepress ${BASENAME}
# commit SVN
#	svn ci -m "update YRHXS.pdf" YRHXS.pdf
#	svn ci -m "update YRHXS.ps " YRHXS.ps
#	svn ci -m "update YRHXS.log" YRHXS.log
#	svn ci -m "update YRHXS.dvi" YRHXS.dvi
#	svn ci -m "update YRHXS.bbl" YRHXS.bbl
#	svn ci -m "update YRHXS.blg" YRHXS.blg
#	svn ci -m "update YRHXS.aux" YRHXS.aux
#	echo "Successfully committed LaTeX outputs to SVN."

testpdflatex:
	pdflatex  ${BASENAME}
	pdflatex  ${BASENAME}
	bibtex    ${BASENAME}
	pdflatex  ${BASENAME}
	pdflatex  ${BASENAME}

#
# standard Latex targets
#

%.dvi:	%.tex 
	$(LATEX) $<

%.bbl:	%.tex *.bib
	$(LATEX) $*
	$(BIBTEX) $*

%.ps:	%.dvi
	$(DVIPS) $< -o $@

%.pdf:	%.tex
	$(PDFLATEX) $<

.PHONY: clean

clean:
	rm -f *.aux *.log *.bbl *.blg *.brf *.cb *.ind *.idx *.ilg *.inx *.dvi *.toc *.out *~ ~* spellTmp 

