\section{ttH Process\footnote{C.~Collins-Tooth, C.~Neu, L.~Reina,
M.~Spira (eds.); S.~Dawson, S.~Dean, S.~Dittmaier, M.~Kr\"amer,
C.T.~Potter, D.~Wackeroth}}

%\vskip 0.8cm

%\noindent
%{\sc C.~Collins-Tooth$^1$, S.~Dawson$^2$, S.~Dean$^3$,
%S.~Dittmaier$^4$, M.~Kr\"amer$^5$, C.~Neu$^6$, C.T.~Potter$^7$,
%L.~Reina$^8$, M.~Spira$^9$ and D.~Wackeroth$^{10}$}

%\vskip 0.8cm

%\begin{small}
%\noindent
%{\it \small
%$^1$ University of Glasgow, Department of Physics and Astronomy, Glasgow
%G12 8QQ, United Kingdom \\
%$^2$ Department of Physics, Brookhaven National Laboratory, Upton, NY
%11973, USA \\
%$^3$ University College London, Department of Physics and Astronomy,
%Gower Street, London WC1E 6BT, United Kingdom \\
%$^4$ Physikalisches Institut, Albert-Ludwigs-Universit\"at Freiburg,
%D--79104 Freiburg, Germany \\
%$^5$ Institut f\"ur Theoretische Physik, RWTH Aachen University,
%D--52056 Aachen, Germany \\
%$^6$ University of Virginia, Charlottesville, VA 22906, USA \\
%$^7$ University of Oregon, Center for High Energy Physics, Eugene, OR
%97403-1274, USA \\
%Street, Montreal, Quebec H3A 2T8, Canada \\
%$^8$ Physics Department, Florida State University, Tallahassee, FL
%32306-4350, USA \\
%$^9$ Paul Scherrer Institut, CH--5232 Villigen PSI, Switzerland \\
%$^{10}$ Department of Physics, SUNY at Buffalo, Buffalo, NY 14260-1500, USA}
%\end{small}
%
%\vskip 0.8cm

\noindent
Higgs radiation off top quarks $\PQq\PAQq/\Pg\Pg\to \PH\PQt\PAQt$ (see
Fig.~\ref{fg:lodiatth}) plays a role for light Higgs masses below
$\sim 150$ \UGeV at the LHC. The measurement of the $\PQt\PAQt\PH$
production rate can provide relevant information on the top-Higgs Yukawa
coupling.  The LO cross section was computed a long time ago
\cite{Raitio:1978pt,Ng:1983jm,Kunszt:1984ri,Gunion:1991kg,Marciano:1991qq}.
These LO results are plagued by large theoretical uncertainties due to
the strong dependence on the renormalization scale of the strong
coupling constant and on the factorization scales of the parton densities
functions inside the proton, respectively. For the LO cross section
there are several public codes available as e.g.~HQQ
\cite{Spira:1997dg,HQQ}, Madgraph/Madevent
\cite{Stelzer:1994tk,Madgraph}, MCFM \cite{MCFM} or PYTHIA \cite{PYTHIA}.
The dominant background processes for this signal process are
$\PQt\PAQt\PQb\PAQb$, $\PQt\PAQt jj$ and $\PQt\PAQt\PGg\PGg$ production
depending on the final-state Higgs boson decay.
\begin{figure}[htb]
\begin{center}
\SetScale{0.8}
\begin{picture}(130,90)(0,0)
\ArrowLine(0,100)(50,50)
\ArrowLine(50,50)(0,0)
\Gluon(50,50)(100,50){3}{5}
\ArrowLine(100,50)(120,70)
\ArrowLine(120,70)(150,100)
\ArrowLine(150,0)(100,50)
\DashLine(120,70)(150,70){5}
\Vertex(50,50){2}
\Vertex(100,50){2}
\Vertex(120,70){2}
\put(-10,78){$\PQq$}
\put(-10,-2){$\PAQq$}
\put(125,53){$\PH$}
\put(125,78){$\PQt$}
\put(125,-2){$\PAQt$}
\end{picture}
\begin{picture}(130,90)(-80,0)
\Gluon(0,0)(50,0){3}{5}
\Gluon(0,100)(50,100){3}{5}
\ArrowLine(100,0)(50,0)
\ArrowLine(50,0)(50,50)
\ArrowLine(50,50)(50,100)
\ArrowLine(50,100)(100,100)
\DashLine(50,50)(100,50){5}
\Vertex(50,100){2}
\Vertex(50,50){2}
\Vertex(50,0){2}
\put(85,35){$\PH$}
\put(-15,78){$\Pg$}
\put(-15,-2){$\Pg$}
\put(85,78){$\PQt$}
\put(85,-2){$\PAQt$}
\end{picture}
\end{center}
\caption{\it \label{fg:lodiatth} Examples of LO Feynman diagrams for the
partonic processes $\PQq\PAQq,\Pg\Pg\to\PQt\PAQt\PH$.}
\end{figure}

The full NLO QCD corrections to $\PQt\PAQt\PH$ production have been
calculated resulting in a moderate increase of the total cross section
by $\sim 20\%$ at the LHC
\cite{Beenakker:2001rj,Beenakker:2002nc,Reina:2001sf,Dawson:2002tg}. The residual scale dependence
has decreased from ${\cal O}(50\%)$ to a level of ${\cal O}(10\%)$ at NLO
thus signalling a significant improvement of the theoretical prediction
at NLO. The full NLO results confirm former estimates based on an
effective Higgs approximation \cite{Dawson:1997im} which approximates Higgs
radiation as a fragmentation process in the high-energy limit. The NLO
effects on the relevant parts of final state particle distribution
shapes are of moderate size, i.e.~${\cal O}(10\%)$, so that former
experimental analyses are not expected to change much due to these
results. There is no public NLO code for the signal process available
yet.

Recently the NLO QCD corrections to the $\PQt\PAQt\PQb\PAQb$ production
background have been calculated
\cite{Bredenstein:2009aj,Bredenstein:2008zb,Bredenstein:2010rs,
Bevilacqua:2009zn,Binoth:2010ra}.  By choosing
$\mu_R^2=\mu_F^2=\Mt\sqrt{p_{T\PQb}p_{T\PAQb}}$ as the central
renormalization and factorization scales the NLO corrections increase
the background cross section within the signal region by about 20--30\%.
Nevertheless the scale dependence is significantly reduced to a level
significantly below 30\%. In addition the signal process $\Pp\Pp\to
\PQt\PAQt\PH\to \PQt\PAQt\PQb\PAQb$ has been added to these background
calculations in the narrow-width approximation \cite{Binoth:2010ra}.
This makes it possible for the first time to study the signal and
background processes including the final-state Higgs decay into
$\PQb\PAQb$ with cuts at the same time at NLO. For highly boosted Higgs
bosons the shapes of the background distributions are affected by the
QCD corrections which thus have to be taken into account properly. The
effects of a jet veto for the boosted-Higgs regime require further
detailed investigations. Very recently the NLO QCD corrections to
$\PQt\PAQt jj$ production have been calculated \cite{Bevilacqua:2010ve}.
However, a full numerical analysis of these results has not been
performed so far.  As it is the case for the signal process, there is no
public code available for the NLO calculations of the background
processes $\PQt\PAQt\PQb\PAQb, \PQt\PAQt jj$.

In the following we will provide results for the inclusive NLO signal
cross section for different values of Higgs masses. The central scale
has been chosen as $\mu_R=\mu_F=\mu_0=\Mt+\MH/2$. In addition, the
uncertainties due to scale variations of a factor of two around the
central scale $\mu_0$ as well as the 68\% C.L. uncertainties due to the
PDFs and the strong coupling $\alpha_s$ are given explicitly. In this
study we did not examine the parametric uncertainties due to the
experimental error on the top mass $\Mt$. We have used the MSTW2008
\cite{Martin:2009iq,Martin:2009bu}, CTEQ6.6 \cite{Pumplin:2002vw} and
NNPDF \cite{Ball:2010de} sets of parton density functions.  The central
values of the strong coupling constant have been implemented according
to the corresponding PDFs for the sake of consistency.
%At LO the $\alpha_s$ values are given by $\alpha_s(\MZ)=0.1394$ for
%MSTW2008 and $\alpha_s(\MZ)=0.130$ for CTEQ6L1, while at NLO the strong
%couplings are chosen as $\alpha_s(\MZ)=0.1202$ for MSTW2008,
%$\alpha_s(\MZ)=0.118$ for CTEQ6.6 and $\alpha_s(\MZ)=0.119$ for NNPDF.
In Table \ref{tb:siglo} we show the LO cross sections for the signal
process and their respective scale and PDF uncertainties calculated with
MSTW2008 PDFs (which are very similar to those obtained with CTEQ6L1
PDFs). It is remarkable that the numbers using the LO PDFs of MSTW2008
and CTEQ6L1 differ by about 20\%. The scale uncertainties at LO are
typically of the order of 30--40\%, while the PDF uncertainties amount
to about 2--3\%.
\begin{table}[hbt]
\caption{\it \label{tb:siglo} LO cross sections of
$\Pp\Pp\to\PQt\PAQt\PH$ for $\sqrt{s}=7$ \UTeV using MSTW2008 and CTEQ6.6
PDFs. The scale dependence is given for the scale variation $\mu_0/2 <
\mu_R,\mu_F < 2\mu_0]$ with $\mu_0=\Mt+\MH/2$. The PDF uncertainties are
defined at 68\% C.L. using MSTW2008. The numbers in brackets denote the
integration errors.}
\begin{tabular}{ccccc} \hline
$\MH$ [\UGeV] & LO [fb], MSTW2008 & LO [fb], CTEQ6L1 & scale & PDF \\ \hline
90  & 213.17(9) & 174.15(1)  & +40.0\%,--26.3\% & +2.51\%,--2.57\% \\
100 & 162.70(7) & 132.950(7) & +39.9\%,--26.3\% & +2.51\%,--2.56\% \\
110 & 126.06(6) & 102.808(6) & +39.9\%,--26.2\% & +2.52\%,--2.54\% \\
120 & 98.66(4)  & 80.428(5)  & +39.8\%,--26.2\% & +2.51\%,--2.55\% \\
130 & 78.09(3)  & 63.62(3)   & +39.8\%,--26.2\% & +2.52\%,--2.54\% \\
140 & 62.43(3)  & 50.788(3)  & +39.9\%,--26.2\% & +2.52\%,--2.56\% \\
150 & 50.35(2)  & 40.940(2)  & +39.8\%,--26.2\% & +2.55\%,--2.57\% \\
160 & 40.98(2)  & 33.293(2)  & +39.8\%,--26.2\% & +2.56\%,--2.59\% \\
170 & 33.62(1)  & 27.30(1)   & +39.8\%,--26.2\% & +2.59\%,--2.59\% \\
180 & 27.83(1)  & 22.568(1)  & +39.8\%,--26.2\% & +2.61\%,--2.61\% \\
190 & 23.20(1)  & 18.800(7)  & +39.8\%,--26.2\% & +2.65\%,--2.64\% \\
200 & 19.481(8) & 15.777(6)  & +39.9\%,--26.2\% & +2.69\%,--2.67\% \\ \hline
\end{tabular}
\end{table}

In Table \ref{tb:mstw} the NLO signal cross section is listed
including the scale, $\alpha_s$ and PDF uncertainties at 68\% C.L. for
MSTW2008 PDFs. It should be noted that the LO and NLO cross sections are
very similar so that the K-factor is about unity for the central scale
choice with MSTW2008 PDFs. The scale uncertainties amount to 5--10\% at
NLO typically, while the PDF uncertainties range at the level of 3--5\%.
The uncertainties induced by the strong coupling $\alpha_s$ turn out to
be of ${\cal O}(2-3\%)$ for MSTW2008 PDFs, while the combined
PDF+$\alpha_s$ errors range at the level of 4--6\%. In Table
\ref{tb:cteq} we show the corresponding NLO numbers for the CTEQ6.6 PDFs
and in Table \ref{tb:nnpdf} for the NNPDF parton densities. The
difference of about 20\% between MSTW2008 and CTEQ6L1 at LO reduces to a
level of 7--8\% at NLO between MSTW2008 and CTEQ6.6. The PDF and
$\alpha_s$ uncertainties are larger with CTEQ6.6 PDFs than with
MSTW2008. For the NNPDF sets we obtain the smallest $\alpha_s$
uncertanties. The PDF uncertainties are comparable to MSTW2008.
\begin{table}[hbt]
\caption{\it \label{tb:mstw} LO and NLO cross sections of $\Pp\Pp\to\PQt
\PAQt\PH$ for $\sqrt{s}=7$ \UTeV using MSTW2008 PDFs. The scale dependence
is given for the scale variation $\mu_0/2 < \mu_R,\mu_F < 2\mu_0$ with
$\mu_0=\Mt+\MH/2$. The $\alpha_s$ and PDF uncertainties are
defined at 68\% C.L. The last column contains the combined
PDF+$\alpha_s$ uncertainties obtained with combined PDF sets. The
numbers in brackets denote the integration errors.}
\begin{tabular}{ccccccc} \hline
$\MH$ [\UGeV] & LO [fb] & NLO [fb] & scale & $\alpha_s$ & PDF & PDF+$\alpha_s$ \\ \hline
 90 & 213.17(9) & 224.8(3) & +4.1\%,--9.7\%  & +2.2\%,--2.7\% & +2.9\%,--3.4\% & +4.2\%,--3.9\% \\
 95 & 186.11(8) & 195.6(2) & +4.0\%,--9.6\%  & +2.2\%,--2.7\% & +2.9\%,--3.4\% & +4.3\%,--3.9\% \\
100 & 162.70(7) & 170.4(2) & +3.9\%,--9.6\%  & +2.2\%,--2.7\% & +2.9\%,--3.4\% & +4.3\%,--3.9\% \\
105 & 143.06(6) & 149.0(2) & +3.7\%,--9.5\%  & +2.2\%,--2.7\% & +2.9\%,--3.4\% & +4.3\%,--3.9\% \\
110 & 126.06(6) & 130.8(2) & +3.6\%,--9.5\%  & +2.2\%,--2.7\% & +2.9\%,--3.4\% & +4.3\%,--3.9\% \\
115 & 111.38(5) & 115.0(1) & +3.5\%,--9.4\%  & +2.2\%,--2.7\% & +3.0\%,--3.4\% & +4.3\%,--3.9\% \\
120 &  98.66(4) & 101.4(1) & +3.4\%,--9.4\%  & +2.2\%,--2.7\% & +3.0\%,--3.4\% & +4.3\%,--3.9\% \\
125 &  87.66(4) &  89.8(1) & +3.3\%,--9.3\%  & +2.2\%,--2.7\% & +3.0\%,--3.4\% & +4.3\%,--3.9\% \\
130 &  78.09(3) & 79.57(8) & +3.2\%,--9.3\%  & +2.2\%,--2.7\% & +3.0\%,--3.3\% & +4.3\%,--3.9\% \\
135 &  69.71(3) & 70.75(7) & +3.1\%,--9.2\%  & +2.2\%,--2.7\% & +3.0\%,--3.4\% & +4.3\%,--3.9\% \\
140 &  62.43(3) & 63.06(6) & +3.0\%,--9.2\%  & +2.2\%,--2.7\% & +3.0\%,--3.4\% & +4.4\%,--3.9\% \\
145 &  55.96(2) & 56.50(6) & +2.9\%,--9.1\%  & +2.2\%,--2.7\% & +3.1\%,--3.4\% & +4.4\%,--3.9\% \\
150 &  50.35(2) & 50.59(6) & +2.9\%,--9.1\%  & +2.2\%,--2.7\% & +3.1\%,--3.4\% & +4.4\%,--3.9\% \\
155 &  45.37(2) & 45.49(5) & +2.8\%,--9.1\%  & +2.2\%,--2.7\% & +3.1\%,--3.4\% & +4.4\%,--3.9\% \\
160 &  40.98(2) & 41.01(4) & +2.8\%,--9.1\%  & +2.2\%,--2.7\% & +3.1\%,--3.4\% & +4.4\%,--3.9\% \\
165 &  37.09(1) & 36.99(3) & +2.7\%,--9.1\%  & +2.2\%,--2.6\% & +3.2\%,--3.4\% & +4.5\%,--3.9\% \\
170 &  33.62(1) & 33.47(3) & +2.7\%,--9.0\%  & +2.2\%,--2.6\% & +3.2\%,--3.4\% & +4.5\%,--3.9\% \\
175 &  30.56(1) & 30.31(3) & +2.6\%,--9.0\%  & +2.2\%,--2.6\% & +3.2\%,--3.4\% & +4.5\%,--3.9\% \\
180 &  27.83(1) & 27.55(3) & +2.6\%,--9.0\%  & +2.2\%,--2.7\% & +3.2\%,--3.4\% & +4.6\%,--4.0\% \\
185 &  25.38(1) & 25.09(3) & +2.6\%,--9.0\%  & +2.2\%,--2.7\% & +3.3\%,--3.5\% & +4.6\%,--4.0\% \\
190 &  23.20(1) & 22.93(3) & +2.6\%,--9.0\%  & +2.2\%,--2.7\% & +3.3\%,--3.5\% & +4.6\%,--4.0\% \\
195 & 21.247(8) & 20.94(2) & +2.6\%,--9.0\%  & +2.2\%,--2.7\% & +3.4\%,--3.5\% & +4.7\%,--4.0\% \\
200 & 19.481(8) & 19.20(2) & +2.6\%,--9.1\%  & +2.2\%,--2.7\% & +3.4\%,--3.6\% & +4.7\%,--4.1\% \\
210 & 16.492(7) & 16.23(2) & +2.8\%,--9.2\%  & +2.2\%,--2.7\% & +3.5\%,--3.7\% & +4.8\%,--4.1\% \\
220 & 14.040(6) & 13.81(1) & +2.9\%,--9.3\%  & +2.2\%,--2.7\% & +3.6\%,--3.7\% & +4.9\%,--4.2\% \\
230 & 12.037(5) & 11.86(1) & +3.2\%,--9.4\%  & +2.3\%,--2.7\% & +3.7\%,--3.9\% & +5.0\%,--4.3\% \\
240 & 10.384(5) & 10.24(1) & +3.2\%,--9.5\%  & +2.3\%,--2.7\% & +3.8\%,--4.0\% & +5.2\%,--4.4\% \\
250 &  9.011(4) & 8.899(9) & +3.5\%,--9.7\%  & +2.3\%,--2.7\% & +4.0\%,--4.1\% & +5.3\%,--4.5\% \\
260 &  7.850(4) & 7.777(10)& +3.9\%,--9.9\%  & +2.3\%,--2.8\% & +4.1\%,--4.3\% & +5.5\%,--4.6\% \\
270 &  6.888(3) & 6.866(8) & +4.3\%,--10.1\% & +2.4\%,--2.8\% & +4.2\%,--4.4\% & +5.6\%,--4.7\% \\
280 &  6.075(3) & 6.092(9) & +4.7\%,--10.4\% & +2.4\%,--2.8\% & +4.4\%,--4.6\% & +5.8\%,--4.9\% \\
290 &  5.376(3) & 5.405(7) & +5.2\%,--10.6\% & +2.4\%,--2.8\% & +4.6\%,--4.7\% & +6.0\%,--5.0\% \\
300 &  4.780(3) & 4.848(7) & +5.6\%,--10.9\% & +2.5\%,--2.9\% & +4.7\%,--4.9\% & +6.2\%,--5.2\% \\ \hline
\end{tabular}
\end{table}

\begin{table}[hbt]
\caption{\it \label{tb:cteq} LO and NLO cross sections of $\Pp\Pp\to\PQt
\PAQt\PH$ for $\sqrt{s}=7$ \UTeV using CTEQ6.6 PDFs. The scale dependence
is given for the scale variation $\mu_0/2 < \mu_R,\mu_F < 2\mu_0$ with
$\mu_0=\Mt+\MH/2$. The $\alpha_s$ and PDF uncertainties are
defined at 68\% C.L. The numbers in brackets denote the integration
errors.}
\begin{tabular}{cccccc} \hline
$\MH$ [\UGeV] & LO [fb] & NLO [fb] & scale & $\alpha_s$ & PDF
\\ \hline
 90 & 174.15(1)  & 210.0(1)  &  +4.2\%,--9.4\% & +3.5\%,--2.5\% &  +5.9\%,--5.1\% \\
 95 & 151.903(9) & 182.49(8) &  +4.1\%,--9.4\% & +3.5\%,--2.5\% &  +5.9\%,--5.1\% \\
100 & 132.950(7) & 159.08(7) &  +4.0\%,--9.3\% & +3.5\%,--2.5\% &  +6.0\%,--5.1\% \\
105 & 116.734(7) & 139.33(6) &  +3.8\%,--9.2\% & +3.5\%,--2.5\% &  +6.1\%,--5.2\% \\
110 & 102.808(6) & 122.09(5) &  +3.7\%,--9.2\% & +3.6\%,--2.5\% &  +6.1\%,--5.2\% \\
115 &  90.806(5) & 107.50(5) &  +3.6\%,--9.2\% & +3.5\%,--2.5\% &  +6.2\%,--5.2\% \\
120 &  80.428(5) &  94.91(4) &  +3.5\%,--9.1\% & +3.5\%,--2.6\% &  +6.2\%,--5.3\% \\
125 &  71.44(3)  &  83.94(4) &  +3.5\%,--9.1\% & +3.6\%,--2.5\% &  +6.3\%,--5.3\% \\
130 &  63.62(3)  &  74.54(3) &  +3.4\%,--9.0\% & +3.6\%,--2.5\% &  +6.4\%,--5.3\% \\
135 &  56.77(2)  &  66.32(3) &  +3.3\%,--9.0\% & +3.6\%,--2.5\% &  +6.4\%,--5.4\% \\
140 &  50.788(3) &  59.16(3) &  +3.2\%,--9.0\% & +3.6\%,--2.5\% &  +6.5\%,--5.4\% \\
145 &  45.547(2) &  52.92(2) &  +3.2\%,--8.9\% & +3.6\%,--2.5\% &  +6.6\%,--5.5\% \\
150 &  40.940(2) &  47.45(2) &  +3.1\%,--8.9\% & +3.6\%,--2.5\% &  +6.6\%,--5.5\% \\
155 &  36.879(2) &  42.60(2) &  +3.1\%,--8.9\% & +3.6\%,--2.5\% &  +6.7\%,--5.6\% \\
160 &  33.293(2) &  38.38(2) &  +3.0\%,--8.9\% & +3.6\%,--2.6\% &  +6.8\%,--5.6\% \\
165 &  30.118(2) &  34.68(2) &  +3.0\%,--8.9\% & +3.7\%,--2.6\% &  +6.9\%,--5.7\% \\
170 &  27.30(1)  &  31.38(1) &  +3.0\%,--8.9\% & +3.7\%,--2.6\% &  +7.0\%,--5.7\% \\
175 &  24.81(1)  &  28.47(1) &  +3.0\%,--8.9\% & +3.7\%,--2.6\% &  +7.0\%,--5.8\% \\
180 &  22.568(1) &  25.88(1) &  +3.0\%,--8.9\% & +3.7\%,--2.6\% &  +7.1\%,--5.8\% \\
185 &  20.577(8) &  23.56(1) &  +3.0\%,--8.9\% & +3.7\%,--2.6\% &  +7.2\%,--5.9\% \\
190 &  18.800(7) &  21.52(1) &  +3.0\%,--8.9\% & +3.8\%,--2.6\% &  +7.3\%,--6.0\% \\
195 &  17.202(7) &  19.70(1) &  +3.0\%,--8.9\% & +3.8\%,--2.6\% &  +7.4\%,--6.0\% \\
200 &  15.777(6) & 18.064(8) &  +3.1\%,--9.0\% & +3.8\%,--2.6\% &  +7.5\%,--6.1\% \\
210 &  13.329(6) & 15.272(7) &  +3.2\%,--9.1\% & +3.9\%,--2.6\% &  +7.8\%,--6.3\% \\
220 &  11.321(5) & 13.019(6) &  +3.3\%,--9.1\% & +3.9\%,--2.6\% &  +8.0\%,--6.4\% \\
230 &   9.696(4) & 11.202(5) &  +3.5\%,--9.3\% & +4.0\%,--2.7\% &  +8.3\%,--6.6\% \\
240 &   8.344(4) &  9.685(5) &  +3.6\%,--9.4\% & +4.1\%,--2.7\% &  +8.5\%,--6.8\% \\
250 &   7.227(3) &  8.450(4) &  +3.9\%,--9.6\% & +4.2\%,--2.7\% &  +8.8\%,--7.0\% \\
260 &   6.286(3) &  7.418(4) &  +4.1\%,--9.7\% & +4.3\%,--2.8\% &  +9.1\%,--7.2\% \\
270 &   5.501(2) &  6.541(4) &  +4.4\%,--9.9\% & +4.4\%,--2.8\% &  +9.5\%,--7.4\% \\
280 &   4.837(2) &  5.809(3) & +4.6\%,--10.1\% & +4.5\%,--2.9\% &  +9.8\%,--7.7\% \\
290 &   4.267(2) &  5.186(3) & +4.9\%,--10.3\% & +4.6\%,--2.9\% & +10.1\%,--7.9\% \\
300 &   3.785(2) &  4.653(3) & +5.2\%,--10.5\% & +4.7\%,--3.0\% & +10.5\%,--8.2\% \\ \hline
\end{tabular}
\end{table}

\begin{table}[hbt]
\caption{\it \label{tb:nnpdf} NLO cross sections of
$\Pp\Pp\to\PQt\PAQt\PH$ for $\sqrt{s}=7$ \UTeV using NNPDF PDFs. The scale
dependence is given for the scale variation $\mu_0/2 < \mu_R,\mu_F <
2\mu_0$ with $\mu_0=\Mt+\MH/2$. The $\alpha_s$ and PDF
uncertainties are defined at 68\% C.L.}
\begin{tabular}{cccccc} \hline
$\MH$ [\UGeV] & NLO [fb] & scale & $\alpha_s$ & PDF
\\ \hline
 90 & 221.3    & +4.8\%,--10.7\% & +1.6\%,--2.3\% & $\pm$4.1\% \\
 95 & 192.0    & +4.7\%,--10.6\% & +1.6\%,--2.3\% & $\pm$4.1\% \\
100 & 167.1    & +4.5\%,--10.6\% & +1.6\%,--2.2\% & $\pm$4.1\% \\
105 & 145.9    & +4.4\%,--10.5\% & +1.6\%,--2.2\% & $\pm$4.1\% \\
110 & 127.8    & +4.3\%,--10.4\% & +1.6\%,--2.2\% & $\pm$4.2\% \\
115 & 112.3    & +4.2\%,--10.4\% & +1.6\%,--2.2\% & $\pm$4.2\% \\
120 & 99.01    & +4.1\%,--10.3\% & +1.6\%,--2.2\% & $\pm$4.2\% \\
125 & 87.50    & +4.1\%,--10.2\% & +1.6\%,--2.2\% & $\pm$4.2\% \\
130 & 77.54    & +4.0\%,--10.2\% & +1.6\%,--2.2\% & $\pm$4.2\% \\
135 & 68.89    & +3.9\%,--10.1\% & +1.6\%,--2.1\% & $\pm$4.2\% \\
140 & 61.37    & +3.8\%,--10.1\% & +1.6\%,--2.1\% & $\pm$4.3\% \\
145 & 54.81    & +3.8\%,--10.0\% & +1.6\%,--2.1\% & $\pm$4.3\% \\
150 & 49.07    & +3.7\%,--10.0\% & +1.6\%,--2.1\% & $\pm$4.3\% \\
155 & 44.03    & +3.7\%,--9.9\%  & +1.6\%,--2.1\% & $\pm$4.3\% \\
160 & 39.61    & +3.6\%,--9.9\%  & +1.6\%,--2.1\% & $\pm$4.4\% \\
165 & 35.72    & +3.6\%,--9.9\%  & +1.6\%,--2.1\% & $\pm$4.4\% \\
170 & 32.28    & +3.6\%,--9.9\%  & +1.6\%,--2.1\% & $\pm$4.5\% \\
175 & 29.24    & +3.6\%,--9.9\%  & +1.6\%,--2.1\% & $\pm$4.5\% \\
180 & 26.55    & +3.6\%,--9.8\%  & +1.6\%,--2.1\% & $\pm$4.5\% \\
185 & 24.16    & +3.6\%,--9.8\%  & +1.6\%,--2.1\% & $\pm$4.6\% \\
190 & 22.03    & +3.6\%,--9.9\%  & +1.6\%,--2.0\% & $\pm$4.6\% \\
195 & 20.13    & +3.6\%,--9.9\%  & +1.6\%,--2.0\% & $\pm$4.7\% \\
200 & 18.44    & +3.7\%,--9.9\%  & +1.6\%,--2.0\% & $\pm$4.7\% \\
210 & 15.56    & +3.8\%,--10.0\% & +1.6\%,--2.0\% & $\pm$4.9\% \\
220 & 13.24    & +3.9\%,--10.0\% & +1.6\%,--2.0\% & $\pm$5.0\% \\
230 & 11.35    & +4.1\%,--10.1\% & +1.6\%,--2.0\% & $\pm$5.2\% \\
240 & 9.805    & +4.3\%,--10.3\% & +1.6\%,--2.0\% & $\pm$5.3\% \\
250 & 8.527    & +4.5\%,--10.4\% & +1.6\%,--2.0\% & $\pm$5.5\% \\
260 & 7.465    & +4.8\%,--10.6\% & +1.6\%,--2.1\% & $\pm$5.7\% \\
270 & 6.575    & +5.1\%,--10.7\% & +1.6\%,--2.1\% & $\pm$5.9\% \\
280 & 5.824    & +5.4\%,--10.9\% & +1.6\%,--2.1\% & $\pm$6.1\% \\
290 & 5.187    & +5.7\%,--11.1\% & +1.5\%,--2.1\% & $\pm$6.4\% \\
300 & 4.642    & +6.0\%,--11.3\% & +1.5\%,--2.1\% & $\pm$6.6\% \\
\hline
\end{tabular}
\end{table}

Tables \ref{tb:sig7} and \ref{tb:sig14} contain our final results for
$\sqrt{s}=7\UTeV$ and 14\UTeV, respectively. We exhibit the central
values and the PDF+$\alpha_s$ uncertainties according to the envelope
method of the PDF4LHC recommendation and the relative scale variations
using MSTW2008 PDFs (see Table \ref{tb:mstw} for $\sqrt{s}=7\UTeV$). The
last column displays the total uncertainties by adding the final errors
in quadrature. The cross sections for $\sqrt{s}=14\UTeV$ are 7--10 times
larger than the corresponding values for $\sqrt{s}=7\UTeV$. The total
uncertainties amount to typically 10--15\% apart from Higgs masses
beyond 200\UGeV where they are slightly larger.
\begin{table}[hbt]
\caption{\it \label{tb:sig7} NLO cross sections of
$\Pp\Pp\to\PQt\PAQt\PH$ for $\sqrt{s}=7$ \UTeV obtained according to the
envelope method of the PDF4LHC goup.}
\begin{tabular}{ccccc} \hline
$\MH$ [\UGeV] & NLO [fb] & scale & PDF+$\alpha_s$ & total error \\ \hline
 90 & 212.4 & +4.1\%,--9.7\%  & $\pm 10.0\%$ & +10.8\%,--14.0\% \\
 95 & 184.5 & +4.0\%,--9.6\%  & $\pm 10.0\%$ & +10.8\%,--13.9\% \\
100 & 160.7 & +3.9\%,--9.6\%  & $\pm 10.0\%$ & +10.8\%,--13.9\% \\
105 & 140.5 & +3.7\%,--9.5\%  & $\pm 10.1\%$ & +10.7\%,--13.9\% \\
110 & 123.3 & +3.6\%,--9.5\%  & $\pm 10.1\%$ & +10.7\%,--13.8\% \\
115 & 108.4 & +3.5\%,--9.4\%  & $\pm 10.1\%$ & +10.7\%,--13.8\% \\
120 & 95.64 & +3.4\%,--9.4\%  & $\pm 10.2\%$ & +10.7\%,--13.8\% \\
125 & 84.63 & +3.3\%,--9.3\%  & $\pm 10.2\%$ & +10.7\%,--13.8\% \\
130 & 75.07 & +3.2\%,--9.3\%  & $\pm 10.2\%$ & +10.7\%,--13.8\% \\
135 & 66.77 & +3.1\%,--9.2\%  & $\pm 10.2\%$ & +10.7\%,--13.8\% \\
140 & 59.54 & +3.0\%,--9.2\%  & $\pm 10.3\%$ & +10.7\%,--13.8\% \\
145 & 53.23 & +2.9\%,--9.1\%  & $\pm 10.3\%$ & +10.7\%,--13.8\% \\
150 & 47.72 & +2.9\%,--9.1\%  & $\pm 10.4\%$ & +10.7\%,--13.8\% \\
155 & 42.90 & +2.8\%,--9.1\%  & $\pm 10.4\%$ & +10.8\%,--13.9\% \\
160 & 38.66 & +2.8\%,--9.1\%  & $\pm 10.6\%$ & +10.9\%,--13.9\% \\
165 & 34.92 & +2.7\%,--9.1\%  & $\pm 10.6\%$ & +11.0\%,--14.0\% \\
170 & 31.62 & +2.7\%,--9.0\%  & $\pm 10.8\%$ & +11.1\%,--14.0\% \\
175 & 28.69 & +2.6\%,--9.0\%  & $\pm 10.9\%$ & +11.2\%,--14.1\% \\
180 & 26.10 & +2.6\%,--9.0\%  & $\pm 11.0\%$ & +11.3\%,--14.2\% \\
185 & 23.79 & +2.6\%,--9.0\%  & $\pm 11.1\%$ & +11.4\%,--14.3\% \\
190 & 21.74 & +2.6\%,--9.0\%  & $\pm 11.2\%$ & +11.5\%,--14.4\% \\
195 & 19.90 & +2.6\%,--9.0\%  & $\pm 11.4\%$ & +11.7\%,--14.5\% \\
200 & 18.26 & +2.6\%,--9.1\%  & $\pm 11.5\%$ & +11.8\%,--14.7\% \\
210 & 15.47 & +2.8\%,--9.2\%  & $\pm 11.8\%$ & +12.1\%,--14.9\% \\
220 & 13.21 & +2.9\%,--9.3\%  & $\pm 12.1\%$ & +12.5\%,--15.3\% \\
230 & 11.37 & +3.2\%,--9.4\%  & $\pm 12.5\%$ & +12.9\%,--15.6\% \\
240 & 9.860 & +3.2\%,--9.5\%  & $\pm 12.9\%$ & +13.3\%,--16.0\% \\
250 & 8.611 & +3.5\%,--9.7\%  & $\pm 13.3\%$ & +13.7\%,--16.4\% \\
260 & 7.569 & +3.9\%,--9.9\%  & $\pm 13.7\%$ & +14.2\%,--16.9\% \\
270 & 6.695 & +4.3\%,--10.1\% & $\pm 14.1\%$ & +14.7\%,--17.4\% \\
280 & 5.956 & +4.7\%,--10.4\% & $\pm 14.5\%$ & +15.3\%,--17.9\% \\
290 & 5.327 & +5.2\%,--10.6\% & $\pm 15.0\%$ & +15.9\%,--18.4\% \\
300 & 4.788 & +5.6\%,--10.9\% & $\pm 15.5\%$ & +16.5\%,--18.9\% \\ \hline
\end{tabular}
\end{table}
\begin{table}[hbt]
\caption{\it \label{tb:sig14} NLO cross sections of
$\Pp\Pp\to\PQt\PAQt\PH$ for $\sqrt{s}=14$ \UTeV obtained according to the
envelope method of the PDF4LHC goup.}
\begin{tabular}{ccccc} \hline
$\MH$ [\UGeV] & NLO [fb] & scale & PDF+$\alpha_s$ & total error \\ \hline
 90 & 1438.4 & +6.2\%,--9.3\%  & $\pm 9.5\%$  & +11.4\%,--13.3\% \\
 95 & 1258.6 & +6.1\%,--9.3\%  & $\pm 9.5\%$  & +11.3\%,--13.3\% \\
100 & 1104.9 & +6.1\%,--9.3\%  & $\pm 9.6\%$  & +11.4\%,--13.3\% \\
105 & 973.98 & +6.0\%,--9.3\%  & $\pm 9.6\%$  & +11.3\%,--13.4\% \\
110 & 861.14 & +6.0\%,--9.3\%  & $\pm 9.7\%$  & +11.4\%,--13.4\% \\
115 & 763.62 & +6.0\%,--9.3\%  & $\pm 9.7\%$  & +11.4\%,--13.4\% \\
120 & 679.37 & +5.9\%,--9.3\%  & $\pm 9.7\%$  & +11.4\%,--13.4\% \\
125 & 606.10 & +5.9\%,--9.3\%  & $\pm 9.8\%$  & +11.4\%,--13.5\% \\
130 & 542.44 & +5.9\%,--9.3\%  & $\pm 9.8\%$  & +11.5\%,--13.5\% \\
135 & 486.69 & +5.9\%,--9.3\%  & $\pm 9.9\%$  & +11.5\%,--13.6\% \\
140 & 438.00 & +5.9\%,--9.3\%  & $\pm 9.9\%$  & +11.5\%,--13.6\% \\
145 & 395.25 & +5.9\%,--9.3\%  & $\pm 10.0\%$ & +11.6\%,--13.6\% \\
150 & 357.55 & +5.9\%,--9.3\%  & $\pm 10.0\%$ & +11.6\%,--13.7\% \\
155 & 324.42 & +5.9\%,--9.4\%  & $\pm 10.1\%$ & +11.7\%,--13.7\% \\
160 & 295.09 & +5.9\%,--9.4\%  & $\pm 10.1\%$ & +11.7\%,--13.8\% \\
165 & 269.08 & +6.0\%,--9.4\%  & $\pm 10.2\%$ & +11.8\%,--13.9\% \\
170 & 246.20 & +6.5\%,--9.7\%  & $\pm 10.3\%$ & +12.2\%,--14.2\% \\
175 & 225.58 & +6.6\%,--9.7\%  & $\pm 10.3\%$ & +12.2\%,--14.2\% \\
180 & 207.27 & +6.6\%,--9.8\%  & $\pm 10.4\%$ & +12.3\%,--14.2\% \\
185 & 190.92 & +6.6\%,--9.8\%  & $\pm 10.4\%$ & +12.3\%,--14.3\% \\
190 & 176.33 & +6.7\%,--9.9\%  & $\pm 10.5\%$ & +12.4\%,--14.4\% \\
195 & 163.20 & +6.7\%,--9.9\%  & $\pm 10.5\%$ & +12.5\%,--14.5\% \\
200 & 151.47 & +6.8\%,--10.0\% & $\pm 10.6\%$ & +12.6\%,--14.6\% \\
210 & 131.28 & +7.0\%,--10.1\% & $\pm 10.7\%$ & +12.8\%,--14.7\% \\
220 & 114.76 & +7.2\%,--10.3\% & $\pm 10.9\%$ & +13.1\%,--15.0\% \\
230 & 101.18 & +7.5\%,--10.4\% & $\pm 11.0\%$ & +13.3\%,--15.2\% \\
240 & 89.894 & +7.6\%,--10.6\% & $\pm 11.1\%$ & +13.5\%,--15.3\% \\
250 & 80.447 & +8.0\%,--10.8\% & $\pm 11.3\%$ & +13.8\%,--15.6\% \\
260 & 72.476 & +8.3\%,--11.0\% & $\pm 11.4\%$ & +14.1\%,--15.8\% \\
270 & 65.704 & +8.6\%,--11.2\% & $\pm 11.5\%$ & +14.4\%,--16.1\% \\
280 & 59.897 & +9.0\%,--11.4\% & $\pm 11.7\%$ & +14.7\%,--16.3\% \\
290 & 54.891 & +9.3\%,--11.6\% & $\pm 11.8\%$ & +15.0\%,--16.5\% \\
300 & 50.518 & +9.7\%,--11.8\% & $\pm 11.9\%$ & +15.3\%,--16.8\% \\ \hline
\end{tabular}
\end{table}

In Fig.~\ref{fg:cxn} we show the LO and NLO cross sections for
$\sqrt{s}=7\UTeV$ for the MSTW2008, CTEQ6.6 and NNPDF 2.0 PDF sets
individually. It is clearly visible that the LO and NLO cross sections
nearly coincide for the central scale choice with MSTW2008 PDFs, while
there are corrections of ${\cal O}(20\%)$ with CTEQ6.6 PDFs. At NLO all
3 PDF sets yield consistent values within less than 10\%.
\begin{figure}[htb]
\begin{picture}(130,250)(30,0)
\put(50,-70){\includegraphics[scale=0.6]{YRHXS_ttH/YRHXS_ttH_fig1.eps}}
\end{picture} \\[1.5cm]
\caption{\label{fg:cxn} \it Total production cross sections of
$\Pp\Pp\to\PQt \PAQt\PH + X$ for $\sqrt{s}=7$ \UTeV at LO and NLO for the
different sets of PDFs.}
\end{figure}

The final total cross sections for $\Pp\Pp\to \PQt\PAQt\PH + X$ are
shown in Fig.~\ref{fg:cxntot} for both energies $\sqrt{s}=7,14\UTeV$.
The error bands include the total uncertainties according to the PDF4LHC
recommendation as given in Tables~\ref{tb:sig7} and \ref{tb:sig14}.
\begin{figure}[htb]
\begin{picture}(130,250)(30,0)
\put(50,-70){\includegraphics[scale=0.6]{YRHXS_ttH/YRHXS_ttH_fig2.eps}}
\end{picture} \\[1.5cm]
\caption{\label{fg:cxntot} \it Total production cross sections of
$\Pp\Pp\to\PQt \PAQt\PH + X$ for $\sqrt{s}=7,14$ \UTeV at NLO including
the total uncertainties according to the PDF4LHC recommendation.}
\end{figure}

\clearpage
